## How to create Splynx addon
0. Check some examples: [Skeleton](https://bitbucket.org/splynx/splynx-addon-skeleton/), [Social-Registration](https://bitbucket.org/splynx/splynx-addon-skeleton), [Cashdesk](https://bitbucket.org/splynx/splynx-cashdesk/), [Agents](https://bitbucket.org/splynx/splynx-agents/), [Braintree](https://bitbucket.org/splynx/splynx-braintree-public/) - payment addon
1. Fork Splynx addon skeleton from [https://bitbucket.org/splynx/splynx-addon-skeleton/](https://bitbucket.org/splynx/splynx-addon-skeleton/)
2. Rename all "splynx-addon-skeleton" to "{splynx-your-addon}"
3. Set title of your addon * - [example](https://bitbucket.org/splynx/splynx-addon-skeleton/src/ee3ae20def808eb3e5062be17f406c7ffa4f3567/commands/InstallController.php#lines-30)
4. Set name of you addon * - [example](https://bitbucket.org/splynx/splynx-addon-skeleton/src/ee3ae20def808eb3e5062be17f406c7ffa4f3567/commands/InstallController.php#lines-39)
5. Set Api Permissions - [example](https://bitbucket.org/splynx/splynx-addon-skeleton/src/ee3ae20def808eb3e5062be17f406c7ffa4f3567/commands/InstallController.php#lines-45) - more about [API Permissions](Permissions.md)
6. [optional] Add Entry Points - [example](https://bitbucket.org/splynx/splynx-addon-skeleton/src/ee3ae20def808eb3e5062be17f406c7ffa4f3567/commands/InstallController.php#lines-61) - more about [Entry Points](EntryPoints.md)
7. [optional] Add Additional fields - [example](https://bitbucket.org/splynx/splynx-cashdesk/src/8d26a85f4501efefb8a5e1e6c6f6238e2df9ca81/commands/InstallController.php#lines-71)  - more about [Additional fields](AdditionalFields.md)
8. [optional] Connect DB: [SQLite](SQLite.md), [MySQL](MySQL.md)
9. [optional] Create [additional dataset](dataset.md) on Splynx, and use it on addon (Config / Integrations / Install module)
10. [optional] Add [hooks](hooks.md) to Splynx and process this hooks in addon.
11. [optional] Implement [user authorization](userAuthorization.md) in addon.
12. Prepare configuration [file](https://bitbucket.org/splynx/splynx-addon-skeleton/src/master/config/config.json) , [example 2](https://bitbucket.org/splynx/splynx-cashdesk/src/master/config/config.json) - more about [config file](configFile.md)
13. Install "{splynx-your-addon}" to your development Splynx server, manual you can found on [README.md](https://bitbucket.org/splynx/splynx-addon-skeleton/src/master/README.md) in your forked repo.


## Additional steps for payment addons:
1. [Payment addon example](https://bitbucket.org/splynx/splynx-braintree-public/)
2. Work with [Payment Accounts](PaymentAccounts.md)
3. [Financial Handlers](FinancialHandlers.md)

## Additional features:

1. [Browser communication](browserCommunication.md)
2. [Redis](redis.md)
3. [Using callbacks](usingCallbacks.md) **Available from v.4.1**



Note:
* Addon's title and name must be shorter than 32 symbols. This is due to the length of the fields in some tables where this information is stored. For example: module_config.module or hook.title.
