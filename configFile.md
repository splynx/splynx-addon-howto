## Configuration files

Every addon on Splynx should have own config file - `config/config.json`

## Base config
```
{
    "path": "splynx_addon_skeleton",
    "translate": true,
    "blocks": {
        "api": {
            "title": "API settings",
            "items": {
                "api_domain": {
                    "title": "API domain",
                    "description": "API domain (with http:// or https:// and last slash)"
                },
                "api_key": {
                    "title": "API key",
                    "description": "API key"
                },
                "api_secret": {
                    "title": "API secret",
                    "description": "API secret"
                }
            }
        }
    }
}
```
## Extended with some values
```
{
  "path": "splynx_cashdesk_addon",
  "translate": true,
  "blocks": {
    "api": {
      "title": "API settings",
      "items": {
        "api_domain": {
          "title": "API domain",
          "description": "API domain (with http:// or https:// and last slash)"
        },
        "api_key": {
          "title": "API key",
          "description": "API key"
        },
        "api_secret": {
          "title": "API secret",
          "description": "API secret"
        }
      }
    },
    "setting": {
      "title": "Setting",
      "items": {
        "payment_method_id": {
          "title": "Payment method ID",
          "description": "Create payment method `CashDesk` at `Config / Finance / Payment methods` and enter it's id here. Or simply use existing method id.",
          "default": 1
        },
        "bank_statements_group": {
          "title": "Bank statements group",
          "description": "Set bank statements group",
          "type": "select",
          "values": {
            "month": "Month",
            "day": "Day"
          },
          "default": "month"
        },
        "extend_customers_search": {
          "title": "Extend customers search",
          "type": "boolean",
          "default": true
        },
        "language": {
          "title": "Language",
          "description": "Set language",
          "type": "select",
          "values": {
            "en-US": "English",
            "ru-RU": "Russian",
            "es-ES": "Spain",
            "ar-SA": "Arabic "
          },
          "default": "en-US"
        },
        "enable_custom_logout_time": {
          "title": "Enabled logout time",
          "type": "boolean",
          "default": false
        },
        "logout_time": {
          "title": "Timeout (seconds)",
          "description": "This amount of seconds indicates the time range when administrator will be logged out automatically after his last activity in the system",
          "type": "integer",
          "default": "1800"
        }
      }
    }
  }
}
```

Big set of examples you can found on your splynx server in `/var/www/splynx/settings` folder

## Usage
```
use splynx\helpers\ConfigHelper;

\\\...

$methodId = ConfigHelper::get('payment_method_id');
```

## Types
Type can be `string`, `integer`, `decimal`, `numeric`, `date`, `datetime`, `boolean`, `select`, `multipleSelect`,  `encrypted`, `textarea` 

## Relations
`select` , and `multipleSelect` can have relation
`"relation": 'model'`
[example](https://bitbucket.org/splynx/splynx-agents/src/master/config/config.json): 

```
        "settings": {
            "title": "Settings",
            "items": {
                "categories": {
                    "title": "Paid transaction categories",
                    "description": "Select transaction categories, for which agent commission will be calculated.",
                    "type": "multipleSelect",
                    "relation": "helpers\\relations\\finance\\TransactionCategoriesSelectForAgents"
                }
            }
```

List of helpers:

* `helpers\\relations\\administrators\\AdministratorRolesSelect`
* `helpers\\relations\\administrators\\AdministratorRolesSelect.php`
* `helpers\\relations\\config\\CRMTemplatesSelect`
* `helpers\\relations\\config\\CommentsReminderTemplatesSelect`
* `helpers\\relations\\config\\EmailTemplatesSelect`
* `helpers\\relations\\config\\Ipv6StaticNetworksSelect`
* `helpers\\relations\\config\\MentionTemplatesSelect`
* `helpers\\relations\\config\\PortalTicketColumnSelect`
* `helpers\\relations\\config\\SmsTemplatesSelect`
* `helpers\\relations\\customers\\CustomerFieldsHelper`
* `helpers\\relations\\customers\\CustomerStatusHelper`
* `helpers\\relations\\finance\\BillingTypesSelect`
* `helpers\\relations\\finance\\PaymentsMethodsSelect`
* `helpers\\relations\\finance\\TransactionCategoriesSelectForAgents`
* `helpers\\relations\\leads\\LeadFieldsHelper`
* `helpers\\relations\\leads\\PipelineStagesHelper`
* `helpers\\relations\\scheduling\\ProjectsSelect`
* `helpers\\relations\\tariffs\\AvailableTariffsHelper`

## Per partner configuration

```
{
  "path": "test",
  "translate": true,
  "blocks": {
    "api": {
      "title": "API settings",
      "items": {
        "api_domain": {
          "title": "API domain",
          "description": "API domain (with http:// or https:// and last slash)"
        },
        "api_key": {
          "title": "API key",
          "description": "API key"
        },
        "api_secret": {
          "title": "API secret",
          "description": "API secret"
        }
      }
    },
    "common_settings": {
      "title": "Common settings",
      "items": {
        "splynx_url": {
          "title": "Splynx url",
          "description": "Splynx url (without last slash)"
        },
        "isProduction": {
          "title": "Environment",
          "description": "Environment",
          "type": "select",
          "values": {
            "false": "Sandbox",
            "true": "Production"
          },
          "default": "false"
        }
      }
    },
    "per_partner_options": {
      "title": "Partners settings",
      "items": {
        "clientId": {
          "title": "Client ID",
          "description": "Client ID"
        },
        "currency": {
          "title": "Currency",
          "type": "select",
          "values": {
            "USD": "USD (U.S. Dollar)",
            "EUR": "EUR (Euro)",
            "AUD": "AUD (Australian Dollar)",
            "BRL": "BRL (Brazilian Real)",
            "CAD": "CAD (Canadian Dollar)",
            "CZK": "CZK (Czech Koruna)",
            "DKK": "DKK (Danish Krone)",
            "HKD": "HKD (Hong Kong Dollar)",
            "HUF": "HUF (Hungarian Forint)",
            "ILS": "ILS (Israeli New Sheqel)",
            "JPY": "JPY (Japanese Yen)",
            "MYR": "MYR (Malaysian Ringgit)",
            "MXN": "MXN (Mexican Peso)",
            "NOK": "NOK (Norwegian Krone)",
            "NZD": "NZD (New Zealand Dollar)",
            "PHP": "PHP (Philippine Peso)",
            "PLN": "PLN (Polish Zloty)",
            "GBP": "GBP (Pound Sterling)",
            "RUB": "RUB (Russian Ruble)",
            "SGD": "SGD (Singapore Dollar)",
            "SEK": "SEK (Swedish Krona)",
            "CHF": "CHF (Swiss Franc)",
            "TWD": "TWD (Taiwan New Dollar)",
            "THB": "THB (Thai Baht)"
          },
          "default": "USD"
        }
      }
    }
  }
}
```
Please check `common_settings` and `per_partner_options`

example of usage:

`        $currency = ConfigHelper::get('currency', $partnerId);
`