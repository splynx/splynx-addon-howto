## Additional Fields

Please read about additional fields on [splynx documentation](https://docs.splynx.com/configuration/system/additional_fields)

For creating additional field with addon add this code to **InstallController.php**

    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'customers',
                'name' => 'social_id',
                'title' => 'Social ID',
                'type' => 'string',
                'required' => false,
                'is_add' => true,
            ]
            ];
    }
    
    
    
##     Type
Type can be `string`, `integer`, `decimal`, `numeric`, `date`, `datetime`, `boolean`, `select`, `select_multiple`,  `password`, `file`, `relation`, `relation_multiple`, `add-on`, `ip`, `textarea` 

## Module
"main_module" place where you want to add additional field, and can be

main_module  | Module
------------- | -------------
admins | Administrators
partners | Partners
locations | Locations
api | API keys
customers | Customers
services_internet | Services (Internet)
services_voice | Services (Voice)
services_custom | Services (Recurring)
customer_documents | Customer documents
tariffs_internet | Internet plans
tariffs_voice | Voice plans
tariffs_custom | Recurring tariffs
tariffs_one_time | One-time services
bundle | Bundles
routers | Routers
voice_devices | Voice devices
monitoring | Monitoring
monitoring_oid | SNMP OID
routers_sectors | Router contention
routers_mikrotik | Routers (MikroTik)
cpe | CPE
ipv4_networks | IPv4 Networks
ipv6_networks | IPv6 Networks
ipv4_networks_ip | IPv4 Networks IP
services_bundle | Services (Bundle)
costs | Costs
invoices | Invoices
billing_transactions | Transactions
payments | Payments
requests | Proforma Invoices
inventory_items | Items (Inventory)
inventory_supplier_invoices | Supplier invoices (Inventory)
inventory_products | Products (Inventory)
inventory_suppliers | Suppliers (Inventory)
inventory_vendors | Vendors (Inventory)
ticket | Tickets
instant_message | Messages
portal_news | News
cards_prepaid | Vouchers
cards_refill | Refill cards
services_voice_calls | Voice calls
services_voice_messages | Voice messages
services_voice_data | Voice data
scheduling_project | Projects
scheduling_task | Tasks
leads | Leads
quotes | Quotes
acs_devices | ACS Devices
