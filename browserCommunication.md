## Browser communication

### Send message direct to web

```php
exec("/var/www/splynx/system/script/tools send-notification --admin=$admin->id --message='$message' --addToPanel=1 --time='$date' --type='$type'");
```

- admin - ID of splynx administrator
- message - text of message
- addToPanel - 0 just show message, 1 save to panel and show message
- time - message time
- type - type of message

  type  | color
      ------------- | -------------
  success  | green
  message  | white (grey)
  warning  | yellow
  information  | blue
  error  | red

### Communicate via web socket

Use [browser emitter](https://bitbucket.org/splynx/splynx-addon-helpers/src/master/components/BrowserEventEmitter.php)
helper and [redis session](https://bitbucket.org/splynx/splynx-addon-helpers/src/master/base/RedisSession.php) helper
