## User authorization

### Single user identity

First you need to create class that implements Yii2
yii\web\IdentityInterface ([example for splynx admin user](https://bitbucket.org/splynx/splynx-cashdesk/src/master/models/Administrator.php))

and use it on configs - [example](https://bitbucket.org/splynx/splynx-cashdesk/src/master/config/web.php#lines-10)

```php
//...
    'components' => [
                'user' => [
                'identityClass' => 'app\models\Administrator',
                'idParam' => 'splynx_admin_id',
                'enableAutoLogin' => $params['enable_custom_logout_time'] ? false : true,
                'authTimeout' => empty($params['logout_time']) ? 1800 : (int)$params['logout_time'],//it does not work if enableAutoLogin = true
                'loginUrl' => '/cashdesk/login'
            ],
        
```

User can be logged in automatically if add-on & Splynx is on the same server. You can use any others users identity &
your own user database to authenticate them. Just create your own class and implement Yii2 yii\web\IdentityInterface.

If you need to authenticate your users directly in add-on create you class for login form and login
controller. ([form example](https://bitbucket.org/splynx/splynx-cashdesk/src/master/models/LoginForm.php)
, [controller example](https://bitbucket.org/splynx/splynx-cashdesk/src/master/controllers/SiteController.php))

### Multiple user identity

You can use different identity (guest, customer, administrator, customUser, etc.) for access to each action in any
controller.

Each identity required implement IdentityInterface.

For example ([stripe](https://bitbucket.org/splynx/splynx-stripe/src/master/config/web.php)):

```php
//...
    'components' => [
            'user' => [
                'identityClass' => '\app\models\splynx\Customer',
                'idParam' => 'splynx_customer_id',
                'loginUrl' => '/portal/login/?return=%2Fstripe',
                'enableAutoLogin' => false,
            ],
            'adminUser' => [
                'class' => 'yii\web\User',
                'identityClass' => '\app\models\splynx\Administrator',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/',
                'enableAutoLogin' => false,
            ],
        
```

and use predefined identity in
controller [access control rules](https://bitbucket.org/splynx/splynx-stripe/src/master/controllers/AdminController.php#lines-30):

```php
//...
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'user' => 'adminUser',
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['some-public-action'], // no-auth action
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true, // any other action in controller requires auth
                        'roles' => ['@']
                    ],
                ]
            ]
        ];
    }
```

More [examples](https://bitbucket.org/splynx/workspace/projects/SPLADD) you can find in any addon where implemented user authorization.