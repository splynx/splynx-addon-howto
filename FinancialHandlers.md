## Financial Handlers

When in payment addon you have stored tokens, you can charge invoices with this tokens.

## Path to handlers
`/var/www/splynx/system/external_handlers/finance/charge`

## Example of handler for braintree
[https://bitbucket.org/splynx/splynx-braintree-public/src/master/handler/braintree/run.php](https://bitbucket.org/splynx/splynx-braintree-public/src/master/handler/braintree/run.php)