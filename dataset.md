## Additional dataset on Splynx

Custom dataset can be integrated direct to Splynx and can be related to some entity in Splynx.

As example we will used [Agents](https://bitbucket.org/splynx/splynx-agents/src/master/
) (Finance / Agents) on Splynx

## Creating dataset
`InstallController.php`

```
///...
use splynx\models\console_api\config\integration\ConsoleModuleConfig;
/// ...

    /**
     * @return array
     */
    public function getApiPermissions(): array
    {
        return [
        ///...
                    [
                'controller' => 'api\admin\config\Module~' . $this->getSimpleModuleName(),
                'actions' => ['index', 'add', 'update', 'view'],
            ],
        ///...
      ];
    }


    /**
     * @return string
     */
    protected function getSimpleModuleName(): string
    {
        return 'splynx_mod_agents'; // need to be changed
    }
   
   // set of additional attributes
     /**
     * @return array
     */
    public function getAdditionalFields(): array
    {
        return [
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_name',
                'title' => 'Name',
                'type' => 'string',
                'required' => true,
                'is_add' => true,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_pay_period',
                'title' => 'Paid periods (in months)',
                'type' => 'integer',
                'required' => true,
                'default_value' => 0,
                'is_add' => true,
                'min_length' => 0,
                'default' => 0,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_percent',
                'title' => 'Commission percent',
                'type' => 'decimal',
                'required' => true,
                'default_value' => 0,
                'is_add' => true,
                'min_length' => 0,
                'max_length' => 100,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_fixed_amount',
                'title' => 'Commission fixed amount',
                'type' => 'decimal',
                'required' => true,
                'default_value' => 0,
                'is_add' => true,
                'min_length' => 0,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_email',
                'title' => 'Email',
                'type' => 'string',
                'is_add' => true,
            ],
            [
                'main_module' => 'customers',
                'name' => $this->getModuleName() . '_agent',
                'title' => 'Agent',
                'type' => 'relation',
                'relation_module' => $this->getSimpleModuleName(),
                'required' => false,
            ],
        ];
    }

        
        
        
           /**
     * @inheritdoc
     * @throws Exception
     */
    public function actionIndex()
    {
        $modules = $this->getNewModulesConfig();

        $install = false;
        foreach ($modules as $item) {
            $module = (new ConsoleModuleConfig())->findOne([
                'module' => $item['module'],
                'type' => $item['type'],
            ]);
            if (empty($module)) {
                $install = true;
                unset($item['title_attribute_prefix']);
                $module = new ConsoleModuleConfig($item);

                if (!$module->save()) {
                    exit("Can`t create module " . $item['module'] . "\n");
                }
            }
        }

        parent::actionIndex();

        if ($install) {
            foreach ($modules as $item) {
                $module = (new ConsoleModuleConfig())->findOne([
                    'module' => $item['module'],
                    'type' => $item['type'],
                ]);
                if (empty($module)) {
                    exit("Can`t find module " . $item['module'] . "\n");
                }
                $module->title_attribute_prefix = $item['title_attribute_prefix'];

                if (!$module->update()) {
                    exit("Can`t update module " . $item['module'] . "\n");
                }
            }
        }
    }

    /**
     * Get configurations for the installing modules
     * @return array
     */
    protected function getNewModulesConfig(): array
    {
        return [
            [
                'module' => $this->getSimpleModuleName(),
                'title' => Yii::t('app', 'Agents'),
                'root' => 'controllers\admin\FinanceController',
                'type' => ConsoleModuleConfig::TYPE_SIMPLE,
                'icon' => 'fa-user-secret',
                'title_attribute_prefix' => $this->getSimpleModuleName() . '_name',
            ]
        ];
    }
    
    
    ///...
```

This example from from [agents InstallController ](https://bitbucket.org/splynx/splynx-agents/src/master/commands/InstallController.php)


## Using dataset
File: `models/api/BaseModuleItem.php` - [Source](https://bitbucket.org/splynx/splynx-agents/src/master/models/api/BaseModuleItem.php)

```
<?php

namespace app\models\api;

use splynx\base\ApiResponseException;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;

/**
 * Class BaseModuleItem
 * @package app\models\api
 *
 * @property-read mixed $moduleName
 */
abstract class BaseModuleItem extends BaseActiveApi
{
    public $id;
    public static $apiUrl = 'admin/config/module';

    /**
     * @inheritdoc
     * @param null $id
     * @param array $conditions
     * @return string
     */
    protected function getApiUrl($id = null, $conditions = []): string
    {
        $result = self::$apiUrl . '/' . $this->getModuleName();

        // Set id
        if ($id !== null) {
            $result .= '--' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * Return module name to access
     * @return string
     */
    abstract protected function getModuleName(): string;

    /**
     * Delete service. Method is overwritten because in services we use custom URLs
     * @return bool
     * @throws ApiResponseException
     * @throws InvalidConfigException
     */
    public function delete(): bool
    {
        $result = ApiHelper::getInstance()->delete($this->getApiUrl($this->id), null);

        return $result['result'];
    }
}
```


File: `models/api/Agent.php` - [Source](https://bitbucket.org/splynx/splynx-agents/src/master/models/api/Agent.php)

```
<?php

namespace app\models\api;

use splynx\base\ApiResponseException;
use yii\helpers\ArrayHelper;

/**
 * Class Agent
 * @package app\models\api
 */
class Agent extends BaseModuleItem
{
    public $additional_attributes = [];

    const NAME = self::MODULE_NAME . '_name'; //required
    const PAY_PERIOD = self::MODULE_NAME . '_pay_period';
    const PERCENT = self::MODULE_NAME . '_percent';
    const FIXED_AMOUNT = self::MODULE_NAME . '_fixed_amount';
    const EMAIL = self::MODULE_NAME . '_email';
    const MODULE_NAME = 'splynx_mod_agents'; // need to be changed

    const PAY_PERIOD_INFINITE = 0;

    /**
     * @param array $ids
     * @return Agent[]
     */
    public static function findByIds(array $ids): array
    {
        return (new self())->findAll(['id' => ['IN', $ids]]);
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected function getModuleName(): string
    {
        return self::MODULE_NAME;
    }

    /**
     * @param string $name
     * @return Agent[]
     */
    public static function findByName(string $name): array
    {
        return (new self())->findAll([], [self::NAME => ['LIKE', $name]]);
    }

    /**
     * @return array
     */
    public static function getItemsForSelect(): array
    {
        $result = [];
        foreach (self::getListAll() as $value) {
            $result[$value->id] = $value->additional_attributes[self::NAME];
        }

        return $result;
    }

    /**
     * @param int $id
     * @return Agent|null
     * @throws ApiResponseException
     */
    public static function findByIdStatic(int $id): ?Agent
    {
        return (new self())->findById($id);
    }

}
```
