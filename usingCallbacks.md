## Using callbacks

## Requirements
Splynx Addon Base 2


## Add entry point
Need to create an entry point that will show an item in the select for available payment gateways. In this case you should add it to method **getEntryPoints** in the **commands/InstallController.php** - file:
```
[
    'name' => 'example_entry_point',
    'place' => 'portal',
    'type' => 'action_link',
    'model' => 'OrderServiceRequest',
    'title' => 'Example entry point',
    'url' => '/example/index',
],
```
You can do the same via GUI in the add-on's entry points menu.


## Request params
**$customerId** and **$amount** need to get from the request:
```
$request = Yii::$app->getRequest();
$customerId = $request->get('customer_id');
$amount = $request->get('amount')
```
Values from those fields sending via GET params like:
```
/example/index?customer_id=1&amount=10
```


## Find user
Need to get a user from the request if it doesn't exist - get it from the session:
```
if (($customerId = $request->get('customer_id')) !== null) {
        $user = Customer::findIdentity($customerId);
} else {
    $user = Yii::$app->getUser()->getIdentity();
}
```

## Behaviors
To be able to use requests with the callbacks - need to add access for all methods which will be used for this:
```
public function behaviors()
{
    return [
        'access' => [
            'class' => AccessControl::class,
            'except' => ['index', 'result'],
            ...
        ],
    ];
}
```

## Redirect to callback
To be able to use a redirect - need to call the next method before showing the payment results:

```
 FinanceStatementCallback::sendResult($model->getBankStatement());
```

where **$model** this is an instance of the model which is inherited from **v2/models/finance/payments/BasePaymentModel.php**
