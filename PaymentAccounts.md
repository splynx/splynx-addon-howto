## Payment Accounts
Payment Accounts used on Splynx to store customers payment information (Payment token, Card 4 last digit, Cardholder name, Card expiration date, etc)

## Creating payment account on addon installation
 In file `commands/InstallController.php` - [example](https://bitbucket.org/splynx/splynx-braintree-public/src/711dea12b1494e4fcd37bc865f3334e20748e9ea/commands/InstallController.php#lines-210)
 
```
    public function getPaymentAccounts()
    {
        return [
            [
                'title' => Yii::t('app', 'BrainTree'),
                'field_1' => Yii::t('app', 'BrainTree account'),
                'field_2' => Yii::t('app', 'Payment token'),
                'field_3' => Yii::t('app', 'Card 4 last digit'),
                'field_4' => Yii::t('app', 'Cardholder name'),
                'field_5' => Yii::t('app', 'Card expiration date'),
            ]
        ];
    }
```
## Using payment account
[Example of code](https://bitbucket.org/splynx/splynx-braintree-public/src/master/models/PaymentAccount.php)
