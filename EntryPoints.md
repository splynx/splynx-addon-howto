## Entry Points

Example of usage in **InstallController.php**

    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'skeleton_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'type' => 'menu_link',
                'url' => '%2Fskeleton%2F',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }
    
    
### Place

Place can be "`admin`" or "`portal`" 
   
### Type
Type can be "`menu_link`", "`code`",  `action_link`, and "`tab`" (for admin only)


## Available options: admin, menu_link

place = "**admin**", type = "**menu_link**"

Root  | Menu
------------- | -------------
controllers\admin\TariffsControlle | Tariff plans
controllers\admin\CRMController | CRM
controllers\admin\crm\LeadsController | CRM / Leads
controllers\admin\CustomersController | Customers
controllers\admin\TicketsController | Tickets
controllers\admin\FinanceController | Finance
controllers\admin\InventoryController | Inventory
controllers\admin\NetworkingController | Networking
controllers\admin\networking\RoutersController | Networking / Routers
controllers\admin\networking\CpeController | Networking / CPE (MikroTik)
controllers\admin\networking\ACSController | Networking / TR-069 (ACS)
controllers\admin\networking\VoiceDevicesController | Networking / Voice devices
controllers\admin\networking\MonitoringController | Networking / Hardware
controllers\admin\networking\IPv4Controller | Networking / IPv4 Networks
controllers\admin\networking\IPv6Controller | Networking / IPv6 Networks
controllers\admin\SupportController | Messages
controllers\admin\support\MessagesController | Messages / Mass sending
controllers\admin\SchedulingController | Scheduling
controllers\admin\VoiceController | Voice
controllers\admin\voice\ProcessingController | Voice / Processing
controllers\admin\voice\RatesController | Voice / Rates
controllers\admin\voice\CategoriesController | Voice / Categories
controllers\admin\voice\PrefixesController | Voice / Prefixes
controllers\admin\AdministrationController | Administration
controllers\admin\administration\LogsController | Administration / Logs
controllers\admin\administration\ReportsController | Administration / Reports
controllers\admin\ConfigController | Config
controllers\admin\config\CrmController | Config / CRM
controllers\admin\config\SchedulingController | Config / Scheduling
controllers\admin\config\InventoryController | Config / Inventory
controllers\admin\config\FinanceController | Config / Finance
controllers\admin\config\SupportController | Config / Tickets
controllers\admin\config\NetworkingController | Config / Networking
controllers\admin\config\VoiceController | Config / Voice
controllers\admin\config\ModulesController | Config / Integrations
controllers\admin\config\ToolsController | Config / Tools


## Available options: admin, code
place = "**admin**", type = "**code**"

Root  | Place
------------- | -------------
controllers\admin\DashboardController | Dashboard
controllers\admin\tariffs\InternetController | - Plans / Internet
controllers\admin\tariffs\VoiceController | - Plans / Voice
controllers\admin\tariffs\RecurringController | - Tariffs / Recurring
controllers\admin\customers\AddController | - Customers / Add
controllers\admin\customers\SearchController | - Customers / Search
controllers\admin\customers\ListController--all | - Customers / List [All]
controllers\admin\customers\ListController--online | - Customers / List [Online]
controllers\admin\customers\ViewController--information | - Customers / View [Information]
controllers\admin\customers\ViewController--services | - Customers / View [Services]
controllers\admin\customers\ViewController--cpe | - Customers / View [CPE]
controllers\admin\customers\ViewController--billing-overview | - Customers / View [Billing][Overview]
controllers\admin\customers\ViewController--billing-transactions | - Customers / View [Billing][Transactions]
controllers\admin\customers\ViewController--billing-invoices | - Customers / View [Billing][Invoices]
controllers\admin\customers\ViewController--billing-payments | - Customers / View [Billing][Payments]
controllers\admin\customers\ViewController--billing-requests | - Customers / View [Billing][Proforma Invoices]
controllers\admin\customers\statistics\InternetStatisticsController | - Customers / View [Statistics][Internet]
controllers\admin\customers\statistics\VoiceStatisticsController | - Customers / View [Statistics][Voice]
controllers\admin\customers\ViewController--documents | - Customers / View [Documents]
controllers\admin\customers\cards\GenerateController | -- Customers / Prepaid vouchers / Generate
controllers\admin\customers\cards\SeriesController | -- Customers / Prepaid vouchers / Series
controllers\admin\customers\cards\SearchController | -- Customers / Prepaid vouchers / Search
controllers\admin\customers\cards\ListController | -- Customers / Prepaid vouchers / List
controllers\admin\customers\cards\ViewController | -- Customers / Prepaid vouchers / View
controllers\admin\finance\DashboardController | - Finance / Dashboard
controllers\admin\finance\TransactionsController | - Finance / Transactions
controllers\admin\finance\InvoicesController | - Finance / Invoices
controllers\admin\finance\RequestsController | - Finance / Proforma Invoices
controllers\admin\finance\PaymentsController | - Finance / Payments
controllers\admin\finance\HistoryController | - Finance / History &amp; Preview
controllers\admin\finance\bank-statements\ProcessController | -- Finance / Bank statements / Process
controllers\admin\finance\bank-statements\HistoryController | -- Finance / Bank statements / History
controllers\admin\finance\bank-statements\ViewController | -- Finance / Bank statements / View history
controllers\admin\finance\refill-cards\GenerateController | -- Finance / Refill cards / Generate
controllers\admin\finance\refill-cards\SeriesController | -- Finance / Refill cards / Series
controllers\admin\finance\refill-cards\ViewController | -- Finance / Refill cards / View series
controllers\admin\networking\routers\AddController | -- Networking / Routers / Add
controllers\admin\networking\routers\ListController | -- Networking / Routers / List
controllers\admin\networking\routers\ViewController--information | -- Networking / Routers / View [Information]
controllers\admin\networking\routers\ViewController--sectors | -- Networking / Routers / View [Router contention]
controllers\admin\networking\routers\ViewController--mikrotik | -- Networking / Routers / View [MikroTik]
controllers\admin\networking\routers\ViewController--mikrotikLog | -- Networking / Routers / View [MikroTik log]
controllers\admin\networking\CpeController | - Networking / CPE
controllers\admin\networking\voice-devices\AddController | -- Networking / Voice devices / Add
controllers\admin\networking\voice-devices\ListController | -- Networking / Voice devices / List
controllers\admin\networking\voice-devices\ViewController | -- Networking / Voice devices / View
controllers\admin\networking\monitoring\AddController | -- Networking / Monitoring / Add
controllers\admin\networking\monitoring\ListController | -- Networking / Monitoring / List
controllers\admin\networking\monitoring\ViewController--information | -- Networking / Monitoring / View [Information]
controllers\admin\networking\monitoring\ViewController--oids | -- Networking / Monitoring / View [SNMP OID]
controllers\admin\networking\monitoring\ViewController--logs | -- Networking / Monitoring / View [Logs]
controllers\admin\networking\monitoring\ViewController--graphs | -- Networking / Monitoring / View [Graphs]
controllers\admin\networking\ipv4\AddController | -- Networking / IPv4 Networks / Add
controllers\admin\networking\ipv4\ListController | -- Networking / IPv4 Networks / List
controllers\admin\networking\ipv4\ViewController--viewNetwork | -- Networking / IPv4 Networks / View [View network]
controllers\admin\networking\ipv4\ViewController--networkOverview | -- Networking / IPv4 Networks / View [Network overview]
controllers\admin\support\messages\CreateController | -- Support / Messages / Create
controllers\admin\support\messages\HistoryController | -- Support / Messages / History
controllers\admin\support\tickets\OpenedController | -- Support / Tickets / List new &amp; open
controllers\admin\support\tickets\ClosedController | -- Support / Tickets / List closed
controllers\admin\support\NewsController | - Support / News
controllers\admin\AdministrationController | Administration settings
controllers\admin\ConfigController | Config settings
controllers\admin\config\finance\AccountingCategoriesController | -- Config / Finance / Accounting categories
controllers\admin\config\finance\AccountingBankAccountsController | -- Config / Finance / Accounting bank accounts
controllers\admin\config\finance\AccountingTaxRatesController | -- Config / Finance / Accounting tax rates
controllers\admin\config\finance\EInvoicingTaxRatesController | -- Config / Finance / eInvoicing tax rates
controllers\admin\config\finance\EInvoicingCategoriesController | -- Config / Finance / eInvoicing categories
controllers\admin\config\finance\EInvoicingPaymentMethodsController | -- Config / Finance / eInvoicing payment methods.
controllers\admin\ProfileController--profile | My Profile [Profile]
controllers\admin\ProfileController--customerSearchFields | My Profile [Customer search fields]
controllers\admin\ProfileController--lastLogin | My Profile [Last login]
controllers\admin\LoginController | Login



## Available options: admin, action_link
place = "**admin**", type = "**action_link**"

Root?(model)  | Place
------------- | -------------
ServicesBundle | Bundle services
ServicesInternet | Internet services
ServicesVoice | Voice Services
ServicesCustom | Recurring services
Invoices | Invoices
Payments | Payments
Requests | Proforma Invoices
Transactions | Transactions
Ticket | Tickets
CustomerDocuments | Customer documents
PaymentAccountData | Payment accounts


## Available options: admin, tab
place = "**admin**", type = "**tab**"

Root  | Place
------------- | -------------
controllers\admin\customers\ListController | - Customers / List
controllers\admin\customers\ViewController | - Customers / View
controllers\admin\customers\ViewController--billing | - Customers / View [Billing]
controllers\admin\customers\StatisticsController | - Customers / View [Statistics]
controllers\admin\customers\ViewController--activity | - Customers / View [Activity]
controllers\admin\networking\routers\ViewController | -- Networking / Routers / View
controllers\admin\networking\ipv4\ViewController | -- Networking / IPv4 Networks / View
controllers\admin\networking\monitoring\ViewController | -- Networking / Monitoring / View
controllers\admin\ProfileController | Profile


## Available options: portal, menu_link
place = "**admin**", type = "**menu_link**"

Root  | Place
------------- | -------------
controllers\portal\DashboardController | Dashboard
controllers\portal\StatisticsController | Statistics
controllers\portal\statistics\InternetStatisticsController | Statistics / Internet
controllers\portal\statistics\VoiceStatisticsController | Statistics / Voice
controllers\portal\ServicesController | Services
controllers\portal\services\ServicesController | Services / Services
controllers\portal\services\HardwareController | Services / Hardware
controllers\portal\FinanceController | Finance
controllers\portal\finance\InvoicesController | Finance / Invoices
controllers\portal\finance\PaymentsController | Finance / Payments
controllers\portal\TicketsController | Tickets
controllers\portal\DocumentsController | Documents
controllers\portal\ProfileController | Profile
controllers\portal\LoginController | Login




## Available options: portal, code
place = "**admin**", type = "**code**"

Root  | Place
------------- | -------------
controllers\portal\DashboardController | Dashboard
controllers\portal\statistics\InternetStatisticsController | Statistics / Internet
controllers\portal\statistics\VoiceStatisticsController | Statistics / Voice
controllers\portal\services\ServicesController | Services / Services
controllers\portal\services\HardwareController | Services / Hardware
controllers\portal\finance\InvoicesController | Finance / Invoices
controllers\portal\finance\PaymentsController | Finance / Payments
controllers\portal\TicketsController | Tickets
controllers\portal\DocumentsController | Documents
controllers\portal\ProfileController | Profile
controllers\portal\LoginController | Login


## Available options: portal, action_link
place = "**admin**", type = "** action_link**"

Root?(Model)  | Place
------------- | -------------
Invoices | Invoices
Requests | Proforma Invoices

