## MySQL
For connection to Splynx Mysql db you should create config file: `config/db.php`
With content like on [example](https://bitbucket.org/splynx/splynx-agents/src/master/config/db.php)

```
<?php

use splynx\helpers\ConfigHelper;

$dbConfig = ConfigHelper::getSplynxConfig('mysql')['db'];
$dbConfig['password'] = exec('/var/www/splynx/system/script/security decrypt --hash="' . $dbConfig['password'] . '"');

$config = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $dbConfig['host'] . ';dbname=' . $dbConfig['database'],
    'username' => $dbConfig['username'],
    'password' => $dbConfig['password'],
    'charset' => $dbConfig['charset'],
    'tablePrefix' => 'SAA_',
];

if (YII_ENV_PROD) {
    $config['enableLogging'] = false;
    $config['enableProfiling'] = false;
}

return $config;
```

and use it on configs - [example](https://bitbucket.org/splynx/splynx-agents/src/master/config/common.php)

```
//...
    'components' => [
        'db' => require 'db.php',
        
```

Then you can use data from Splynx as ActiveRecord - [example](https://bitbucket.org/splynx/splynx-agents/src/master/models/db/)


Migrations and custom models also possible to create in Splynx Mysql DB.
