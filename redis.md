## [Redis](https://bitbucket.org/splynx/splynx-addon-helpers/src/master/helpers/RedisHelper.php) in add-ons

### Set in config `config/web.php`

```php
// ...
components' => [
//...
            'redis' => array_merge(
                ['class' => 'yii\redis\Connection'],
                splynx\helpers\ConfigHelper::getRedisConfigForAddOns()
            ),
//...   
        ],
// ...
```

and use via Yii application:

```php
// ...
        /**
         * @var yii\redis\Connection $redis
         */
        $redis = Yii::$app->redis;
        // for example set & get value
        $redis->set('your_key', 'your_value');
        $redis->get('your_key');
        // delete record from redis
        $redis->delete('your_key');
// ...
```
